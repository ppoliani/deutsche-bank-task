export const renderSparkline = (className, data) => {
  const elem = document.querySelector(className);
  Sparkline.draw(elem, data);
}
