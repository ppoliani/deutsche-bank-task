export const renderPrice = value => value.toFixed(2);
export const renderExchangeSymbol = str => `${str.slice(0, 3).toUpperCase()}/${str.slice(3).toUpperCase()}`
