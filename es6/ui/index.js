import {partial, toList} from '../utils/fn';
import {connectToDatastore} from '../data';
import Table from './Table';

const columns = [
  {name: 'Name', key: 'name'},
  {name: 'Bid Price', key: 'bestBid'},
  {name: 'Ask Price', key: 'bestAsk'},
  {name: 'Last Bid Change', key: 'lastChangeBid'},
  {name: 'Last Ask Change', key: 'lastChangeAsk'},
  {name: 'Midprice', key: 'midprice'}
]

export const render = () => {
  connectToDatastore((partial(Table, columns)) ['∘'] (toList))
}
