import {prop, head, entries} from '../utils/fn';
import {sort} from '../data/helpers';
import {renderTo, renderHeader, renderRows} from './common';
import {renderSparkline} from './sparkline';

const getColumnKeys = columns => columns.map(prop('key'))

// get the latest record for each price pair
const getLatestPriceInfo = rows => rows.map(head);

// returns the midprice for the last 30 entries for each price pair
const getChartData = rows => rows.reduce((acc, pairs) => {
  acc[pairs[0].name] = pairs.map(p => (p.bestBid + p.bestAsk) / 2);
  return acc;
}, {})

export default (columns, rows) => {
  const sortData = sort(rows);
  const content = `
    <table>
       <thead>
        <tr>${columns.map(renderHeader ['∘'] (prop('name'))).join('')}</tr>
       </thead>
       <tbody>${renderRows(getColumnKeys(columns), getLatestPriceInfo(sortData))}</tbody>
    </table>
  `

  renderTo('main-table', content);

  const chartData = [...entries(getChartData(rows))];
  chartData.map(([pairName, data]) => {
    return renderSparkline(`.sparkline-${pairName}`, data)
  })
}
