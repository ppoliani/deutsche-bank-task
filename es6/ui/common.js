import {renderPrice, renderExchangeSymbol} from './formatters';
import {partial} from '../utils/fn';

const renderColumn = (name, value) => name === 'name'
  ? `<td>${renderExchangeSymbol(value)}</td>`
  : `<td>${renderPrice(value)}</td>`;

const renderRow = (columns, row, i) => `
  <tr>
    ${
      columns
        .filter(col => col !== 'midprice')
        .map(col => renderColumn(col, row[col])).join('')
    }
    <td class="sparkline-${row['name']}"></td>
  </tr>
`

export const renderHeader = value => `<th>${value}</th>`

export const renderTo = (className, content) => {
  document.querySelector('.main-table').innerHTML = content;
}

export const renderRows = (columns, values) => `
  ${values.map(partial(renderRow, columns)).join('')}
`
