export const compose = (...fns) => {
  const [first, ...funcs] = fns.reverse();
  return (...args) => funcs.reduce((res, fn) => fn(res), first(...args));
};

// composition hack
Function.prototype['∘'] = function(f) {
  return x => this(f(x))
}

export const partial = (fn, ...args) => (...restArgs) => fn.apply(this, args.concat(restArgs));
export const prop = key => obj => obj[key];
export const last = col => col[col.length - 1];
export const head = col => col[0];
export const id = a => a;

export const entries  = function *(obj) {
  for (let key of Object.keys(obj)) {
    yield [key, obj[key]];
  }
}

export const toList = obj => [...entries(obj)].map(([_, val]) => val)
