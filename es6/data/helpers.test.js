import test from 'ava';
import {sort} from './helpers';

test('it should sort by lastChangeBid an array of objects in descending order', t => {
  const data = [
    [{lastChangeBid: 4}, {lastChangeBid: 10}],
    [{lastChangeBid: 10}, {lastChangeBid: 1}]
  ];

  const expected = [
    [{lastChangeBid: 10}, {lastChangeBid: 1}],
    [{lastChangeBid: 4}, {lastChangeBid: 10}],
  ]

  t.deepEqual(sort(data), expected)
});
