import {partial} from '../utils/fn';
import {subscribe} from '../services/ws';

const MAX_ENTRIES = 30;
let store = {}

const updateState = (store, msg) => {
  store[msg.name] = store[msg.name] || [];

  // keep only MAX_ENTRIES records
  if(store[msg.name].length === MAX_ENTRIES) store[msg.name].splice(store[msg.name].length - 1)
  store[msg.name].unshift(msg);
  return store;
};

export const connectToDatastoreRoot = (subscribe, store, next) => subscribe((next) ['∘'] (partial(updateState, store)));
export const connectToDatastore = partial(connectToDatastoreRoot, subscribe, store)
