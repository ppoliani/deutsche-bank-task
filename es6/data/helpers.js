import {head} from '../utils/fn';

export const sort = prices => prices.sort((a, b) => head(b).lastChangeBid - head(a).lastChangeBid);
