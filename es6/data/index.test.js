import test from 'ava';
import td from 'testdouble';
import {id} from '../utils/fn';
import {connectToDatastoreRoot} from './index';

test.beforeEach(t => {
  t.context.data = {name: 'usdgbp'}
  t.context.unsubscribe = td.function('unsubscribe');
  t.context.subscribe = next => {
    next(t.context.data);
    return t.context.unsubscribe;
  };
});


test('connectToDatastore should return an unsubscribe function', t => {
  const subscription = connectToDatastoreRoot(t.context.subscribe, {}, id);

  subscription();

  td.verify(t.context.unsubscribe());
  t.true(true);
});

test('connectToDatastore should update the store with the incoming msg data', t => {
  connectToDatastoreRoot(t.context.subscribe, {}, state => {
    t.deepEqual(state, {
      'usdgbp': [{name: 'usdgbp'}]
    });
  });
});

test('it should not keep more that 30 entries for each price pair', t => {
  const store = {'usdgbp': Array.from(new Array(30), id)}
  connectToDatastoreRoot(t.context.subscribe, store, state => {
    t.is(state['usdgbp'].length, 30);
  });
});
