import {connect} from './services/ws';
import {render} from './ui';

connect()
.then(render)
.catch(error => {
  console.error(`Unexpected error: ${error}`)
});
