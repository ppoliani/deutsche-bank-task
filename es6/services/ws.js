import {prop} from '../utils/fn';

const url = 'ws://localhost:8011/stomp'
let client = {};

client.debug = msg => {
  if (global.DEBUG) {
    console.info(msg)
  }
};

const unpack = msg => msg ? JSON.parse(msg) : {};

export const subscribe = (next, error) => {
  const onMsg = (next) ['∘'] (unpack) ['∘'] (prop('body'));
  const subscription = client.subscribe('/fx/prices', onMsg);

  return () => {
    subscription.unsubscribe();
  }
}

export const connect = () => new Promise((resolve, reject) => {
  client = Stomp.client(url);

  client.connect({}, resolve, error => {
    reject(error.headers.message);
  })
})
