global.DEBUG = false

require('babel-polyfill');
require('./site/index.html')
require('./site/style.css')
require('./es6/')


const exampleSparkline = document.getElementById('example-sparkline')
Sparkline.draw(exampleSparkline, [1, 2, 3, 6, 8, 20, 2, 2, 4, 2, 3])
